# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!--
These are the categories specified by Keep a Changelog. Please pick a suitable
one and don't make up any for yourself.

### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security
-->

## [Unreleased]

### Fixed
- Always re-render tabline (4d8f55d)
- Correctly check if files exist (de4f415)
- Escape pattern chars in path comparison for cwd check (9c6dcd8)
- Restore file extension if no icon can be found (!2)

### Changed
- Rewrite unique tabname determination (136150d)
- Keep track of custom color highlight groups (bd46a09)
- Show an icon for modified/unsaved tabs (ac9ada3)
- Handle empty buffers specially (fb2bbf9)
- Read git dir from buffer first in `fugitive` tabs (!1)

### Added
- Crawl filesystem entries periodically to gather unique filenames (e1fb882)
- Better message logging (467749e)
- Special handling for tabs based on filetype (240015f)
- Shorten unique paths beneath cwd (80e9afa)
- Ellipsize tab names ranging out of the screen (e22149a)
- Character-precise view scrolling (1ce41b7)


## Initial release - 2024-02-09
