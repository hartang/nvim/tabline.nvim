# tabline - A tabline for nvim, without fancy extras

![tabline demo](assets/tabline_demo.png)

<!--toc:start-->
- [tabline - A tabline for nvim, without fancy extras](#tabline-a-tabline-for-nvim-without-fancy-extras)
  - [Features](#features)
  - [Requirements](#requirements)
  - [Installation](#installation)
  - [Configuration](#configuration)
  - [Usage](#usage)
  - [Known Issues](#known-issues)
  - [TODO](#todo)
  - [License](#license)
<!--toc:end-->


## Features

- Suppress paths for unique files in cwd
- Replace file extensions with [colored icons][icons]
- Scroll and ellipsize long tablines
- Prettier tabnames for special buffers (help, fugitive, Fm), user configurable
- Uses default tab-related highlight groups


## Requirements

- A [nerd font][nerdfonts]


## Installation

With [lazy.nvim]:

```lua
{
    url = "https://gitlab.com/hartang/nvim/tabline.nvim",
    dependencies = { "nvim-web-devicons" },
    -- Ensure the plugin is registered on startup, but after e.g. themes are loaded
    lazy = true,
    event = "VeryLazy",
    -- You must enable either of these. Use `config = true` if you want the
    -- default settings, populate `opts` otherwise.
    config = true,
    opts = {
        -- See below
    },
}
```


## Configuration

Here's the list of available settings for the `setup()` function:

```lua
{
    --- If true, replace file extensions in tab titles with icons
    extension_icons = true,
    --- Separator between tabs (doubled)
    tab_separator = "",
    --- Ellipsis to show when tab line truncation is necessary
    tab_line_ellipsis = "..."
    --- Override for special filetypes
    --- This is a table of [filetype] = [string|function(winnr, tabnr):string]
    special_filetypes = {
        -- plain string
        ["Fm"] = " Browsing 󰱽 ",
        -- more sophisticated callback
        ["fugitive"] = function(winnr, tabnr)
            local buflist = vim.fn.tabpagebuflist(tabnr)
            local bufnr = buflist[winnr]
            local gitdir = vim.fn.FugitiveGitDir(bufnr)
            gitdir = vim.fn.fnamemodify(gitdir, ":h:t")
            return " " .. gitdir .. " 󰊢 "
        end,
    },
},
```


## Usage

tabline currently doesn't expose any functions and doesn't create key mappings.
You'll have to provide your own. Here's an example to get you going:

```lua
local opts = { noremap = true, silent = true }

-- Move between tabs
-- Alt+, moves to the previous tab (to the left)
vim.api.nvim_set_keymap("n", "<A-,>", "<Cmd>tabprevious<CR>", opts)
-- Alt+. moves to the next tab (to the right)
vim.api.nvim_set_keymap("n", "<A-.>", "<Cmd>tabnext<CR>", opts)

-- Rearrange tabs in tabline
-- Alt+> moves the current tab to the right
vim.api.nvim_set_keymap("n", "<A->>", "<Cmd>tabmove +<CR>", opts)
-- Alt+< moves the current tab to the left
vim.api.nvim_set_keymap("n", "<A-<>", "<Cmd>tabmove -<CR>", opts)
```


## Known Issues

Depending on how you set your themes for nvim, there might be races between
tabline and your theme plugin. If you experience issues with your theme (e.g.
tab separators have wrong colors, icons don't have a background), try running
`:doautocmd ColorScheme` in a fully loaded vim session.


## TODO

- Improve path shortening
    - Make paths truly unique


## License

This software is licensed under the GNU General Public License v3.0 or any
later version, at your option. You can find a copy of the license at
https://www.gnu.org/licenses/gpl-3.0.txt


[lazy.nvim]: https://github.com/folke/lazy.nvim
[nerdfonts]: https://www.nerdfonts.com/
[icons]: https://github.com/nvim-tree/nvim-web-devicons
