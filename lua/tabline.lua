-- Copyright (C) 2024 Andreas Hartmann <hartan@7x.de>
-- GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
-- SPDX-License-Identifier: GPL-3.0-or-later

-- TODO:
--  * Add a module to deal with highlighting, this can probably be compacted
local StyledText = require("tabline.styled_text")
local LineFragment = require("tabline.line_fragment")
local special = require("tabline.special")
local Colors = require("tabline.colors")
local Log = require("tabline.log")
local Fs = require("tabline.fs")

local M = {}

--- Configurable plugin options.
M.options = {
    --- If true, replace file extensions in tab titles with icons
    extension_icons = true,
    --- Separator between tabs (doubled)
    tab_separator = "",
    --- Ellipsis to show when tab line truncation is necessary
    tab_line_ellipsis = "...",
    --- Override for special filetypes
    special_filetypes = special,
}

--- Overrides to plugin options.
---
--- These are set by the plugin and should not be set by the user. Used for example to override the
--- icons setting.
M.options_override = {}

--- Persistent application state.
M.state = {
    --- Scroll of the tabline, when the entire content is wider than the screen.
    view_scroll = 0,
}

local icons_ok, devicons = pcall(require, "nvim-web-devicons")
if not icons_ok and M.options.extension_icons then
    Log:error("'nvim-web-devicons' not found, cannot display icons")
    M.options_override.extension_icons = false
end

function M.setup(conf)
    M.options = vim.tbl_deep_extend("force", M.options, conf, M.options_override)
    Colors:setup()
    Fs:setup()
    vim.o.tabline = '%!v:lua.nvim_tabline()'
end

--- Get the final arrow component of the tab bar.
---
--- @return LineFragment
local function tab_line_end_regular()
    local styled_text = StyledText:new(M.options.tab_separator, "TabLineSelInverse")
    return LineFragment:new():push(styled_text)
end

--- Get the final component of the ellipsized tab bar.
---
--- Ellipsizing takes place when the tab bar doesn't entirely fit the screen.
---
--- @return LineFragment
local function tab_line_end_ellipsis()
    local ellipsis = StyledText:new(" " .. M.options.tab_line_ellipsis, "TabLine")
    return LineFragment:new():push(ellipsis)
end

--- Get the tab line start
---
--- @return LineFragment
local function tab_line_start_ellipsis()
    local ellipsis = StyledText:new(M.options.tab_line_ellipsis .. " ", "TabLine")
    return LineFragment:new():push(ellipsis)
end

--- Convert a tab number to its tab ribbon.
---
--- @param tabidx number Index of the tab being inspected
--- @param is_current_tab boolean True if this is the current tab
--- @return LineFragment
local function tab_to_ribbon(tabidx, is_current_tab)
    local fragment = LineFragment:new()
    -- active window in tab page index
    local winnr = vim.fn.tabpagewinnr(tabidx)
    -- active buffer for each window in tab page inxed
    local buflist = vim.fn.tabpagebuflist(tabidx)
    -- Get active buffer in tab page index
    local bufnr = buflist[winnr]
    -- Get the name of the active buffer
    local bufname = vim.fn.bufname(bufnr)
    -- true if active buffer has unsaved modifications
    local bufmodified = (vim.fn.getbufvar(bufnr, "&modified", 0) ~= 0)
    -- filetype of the current buffer
    local buf_filetype = vim.fn.getbufvar(bufnr, "&filetype", "")

    -- tabline start arrow
    if is_current_tab then
        fragment:push(StyledText:new(" ", "TabLineSel"))
    else
        fragment:push(StyledText:new(M.options.tab_separator, "TabLineSelInverse"))
    end

    -- tab text styling
    local s = ""
    local s_style = nil
    if is_current_tab then
        s_style = "TabLineSel"
    else
        s_style = "TabLine"
    end
    s = s .. " "

    if bufmodified then
        s = s .. "󰽂 "
    end

    -- TODO(hartan): Handle empty buffers?
    local maybe_special = M.options.special_filetypes[buf_filetype]
    if maybe_special then
        local tabname = ""
        if type(maybe_special) == "function" then
            tabname = "" .. maybe_special(winnr, tabidx)
        elseif type(maybe_special) == "string" then
            tabname = maybe_special
        else
            Log:error(
                "User filetype handlers must be functions or strings.\n" ..
                "Failed to determine tabname for ft '" .. buf_filetype .. "'",
                { once = true }
            )
            tabname = " <INVALID> "
        end
        fragment:push(StyledText:new(tabname, s_style))
    elseif bufname == "" then
        fragment:push(StyledText:new(" <no name> ", s_style))
    else
        bufname = Fs:buffer_to_tabname(bufname)

        if M.options.extension_icons then
            local tabname = vim.fn.fnamemodify(bufname, ":r")
            s = s .. tabname
            fragment:push(StyledText:new(s, s_style))

            local icon = nil
            local color = nil
            local icon_ident = ""
            local bufname_ext = vim.fn.fnamemodify(bufname, ":e")

            if bufname_ext ~= "" then
                icon, color = devicons.get_icon_color(bufname, bufname_ext, { strict = true })
                icon_ident = bufname_ext
            end

            if not icon then
                if buf_filetype ~= "" then
                    icon, color = devicons.get_icon_color_by_filetype(buf_filetype)
                    icon_ident = buf_filetype
                end
            end

            if icon then
                -- Create highlight group for the icon
                local base_hl = "TabLine"
                if is_current_tab then
                    base_hl = base_hl .. "Sel"
                end
                local hl_name = base_hl .. "Icon" .. icon_ident
                local icon_hl = Colors:get_hl(hl_name)

                if icon_hl == nil then
                    local base_hl_table = Colors:get_hl(base_hl)
                    if base_hl_table == nil then
                        Log:error(
                            "can't find highlight group '" .. base_hl ..
                            "' created during plugin setup",
                            { once = true }
                        )
                        base_hl_table = Colors:default_hl()
                    end
                    icon_hl = vim.tbl_deep_extend("force", base_hl_table, { fg = color })
                    Colors:set_hl(hl_name, icon_hl)
                end

                fragment:push(StyledText:new(" " .. icon .. " ", hl_name))
            elseif bufname_ext ~= "" then
                -- No icon but there was an extension at the end initially
                fragment:push(StyledText:new("." .. bufname_ext .. " ", s_style))
            else
                -- No icon and there never was an extension
                fragment:push(StyledText:new(" ", s_style))
            end
        else
            s = s .. bufname .. " "
            fragment:push(StyledText:new(s .. " ", s_style))
        end
    end

    if is_current_tab then
        fragment:push(StyledText:new(" ", "TabLineSel"))
    else
        fragment:push(StyledText:new(M.options.tab_separator, "TabLineSel"))
    end

    return fragment
end

function _G.nvim_tabline()
    -- TODO(hartan): Revise this once https://github.com/neovim/neovim/pull/24137 lands and go back
    -- to event-based rendering (see 46a56c1eeacecab72f2f2c961115d6b60edc77f1)
    local tabs = {}
    local current_tab = vim.fn.tabpagenr()
    local last_tab = vim.fn.tabpagenr("$")
    local total_width = 0

    -- iterate over all tabs
    for index = 1, last_tab do
        local is_current_tab = index == current_tab
        local fragment = tab_to_ribbon(index, is_current_tab)
        local startcol = total_width
        total_width = total_width + fragment.width

        tabs[index] = {
            index = index,
            ribbon = fragment,
            startcol = startcol,
            endcol = startcol + fragment.width,
            width = fragment.width,
            current = is_current_tab,
        }
    end

    local focused_tab = tabs[current_tab]
    local window_width = vim.fn.getbufvar("%", "&columns")
    local s = ""
    if total_width <= window_width then
        -- There's enough space to display all tabs
        for _, value in pairs(tabs) do
            s = s .. value.ribbon:to_string()
        end

        local tab_line_end = tab_line_end_regular()
        if (total_width + tab_line_end.width) <= window_width then
            s = s .. tab_line_end:to_string()
        end

        s = s .. "%#TabLineFill#"
    else
        -- Check if focused tab is visible with current scrolling
        local tab_in_view = function(tab, start_col, end_col)
            return (tab.startcol >= start_col) and (tab.endcol <= end_col)
        end

        -- The new amount of scrolling to apply, if any
        local new_scroll = nil
        -- The maximum amount we can scroll
        local new_max_scroll = tabs[last_tab].endcol - window_width
        -- Clamp scroll stored in state to maximum possible scroll
        if M.state.view_scroll > new_max_scroll then
            M.state.view_scroll = new_max_scroll
        end

        local line_start = LineFragment:new()
        local line_end = LineFragment:new()

        if focused_tab.index == 1 then
            line_end = tab_line_end_ellipsis()
            new_scroll = 0
        elseif focused_tab.index == last_tab then
            line_start = tab_line_start_ellipsis()
            new_scroll = new_max_scroll
        else
            if M.state.view_scroll == 0 then
                -- There *must* be an ellipsis at the end
                line_end = tab_line_end_ellipsis()
                if tab_in_view(focused_tab, 0, (window_width - line_end.width)) then
                    -- No scrolling needed
                else
                    -- Must scroll to the right
                    line_start = tab_line_start_ellipsis()
                    new_scroll = focused_tab.endcol - (window_width - line_end.width)
                end
            elseif M.state.view_scroll == new_max_scroll then
                line_start = tab_line_start_ellipsis()
                if tab_in_view(
                        focused_tab,
                        (new_max_scroll + line_start.width),
                        (new_max_scroll + window_width)
                    ) then
                    -- No scrolling needed
                else
                    -- Must scroll to the left
                    line_end = tab_line_end_ellipsis()
                    new_scroll = focused_tab.startcol - line_start.width
                end
            else
                line_start = tab_line_start_ellipsis()
                line_end = tab_line_end_ellipsis()

                local view_start = M.state.view_scroll + line_start.width
                local view_end = M.state.view_scroll + window_width - line_end.width
                if tab_in_view(focused_tab, view_start, view_end) then
                    -- Do nothing
                elseif focused_tab.startcol < view_start then
                    -- Scroll to the left
                    new_scroll = focused_tab.startcol - line_start.width
                else
                    -- Scroll to the right
                    new_scroll = focused_tab.endcol - (window_width - line_end.width)
                end
            end
        end
        M.state.view_scroll = new_scroll or M.state.view_scroll

        s = s .. line_start:to_string()
        local view_start = M.state.view_scroll + line_start.width
        local view_end = M.state.view_scroll + window_width - line_end.width

        for _, tab in ipairs(tabs) do
            if tab_in_view(tab, view_start, view_end) then
                -- Tab is completely in view
                s = s .. tab.ribbon:to_string()
            elseif (tab.endcol < view_start) then
                -- Not visible
            elseif (tab.startcol > view_end) then
                -- Not visible
            elseif (tab.startcol < view_start) then
                -- Clip from the left
                s = s .. tab.ribbon:to_string_trunc_left(view_start - tab.startcol)
            elseif (tab.endcol > view_end) then
                -- Clip from the right
                s = s .. tab.ribbon:to_string_trunc_right(tab.endcol - view_end)
            end
        end
        s = s .. line_end:to_string()
    end

    return s
end

return M
