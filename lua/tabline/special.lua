-- Copyright (C) 2024 Andreas Hartmann <hartan@7x.de>
-- GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
-- SPDX-License-Identifier: GPL-3.0-or-later

--  Default handlers for special filetypes.
--
--  Keys in this table are filetypes to act on, the values can be either:
--    * A function taking a window number and tab number as argument and returning a string, or
--    * A string
local M = {
    ["Fm"] = " Browsing 󰱽 ",
    ["fugitive"] = function(winnr, tabnr)
        local bufnr = vim.fn.tabpagebuflist(tabnr)[winnr]
        local gitdir = vim.fn.getbufvar(bufnr, "git_dir") or vim.fn.FugitiveGitDir(bufnr)

        return " " .. vim.fn.fnamemodify(gitdir, ":h:t") .. " 󰊢 "
    end,
    ["help"] = function(winnr, tabnr)
        local buflist = vim.fn.tabpagebuflist(tabnr)
        local bufnr = buflist[winnr]
        local bufname = vim.api.nvim_buf_get_name(bufnr)
        bufname = bufname:gsub("^.+/doc/", "")
        bufname = vim.fn.fnamemodify(bufname, ":r")
        return " " .. bufname .. " 󰞋 "
    end,
    ["netrw"] = function(winnr, tabnr)
        local buflist = vim.fn.tabpagebuflist(tabnr)
        local bufnr = buflist[winnr]
        local netrw_dir = vim.fn.getbufvar(bufnr, "netrw_curdir")
        if netrw_dir == "" then
            return " Browsing 󰱽 "
        else
            netrw_dir = vim.fn.fnamemodify(netrw_dir, ":~")
            return " Browsing [" .. vim.fn.pathshorten(netrw_dir, 3) .. "] 󰱽 "
        end
    end,
    ["TelescopePrompt"] = " Picking 󰭎 ",
}

return M
