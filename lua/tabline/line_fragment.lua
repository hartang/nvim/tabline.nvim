-- Copyright (C) 2024 Andreas Hartmann <hartan@7x.de>
-- GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
-- SPDX-License-Identifier: GPL-3.0-or-later

--- @class LineFragment
--- @field content StyledText[] Individual bits of this text
--- @field width number Full width of the content when printed
local LineFragment = {
    content = {},
    width = 0,
}

--- Create a new, empty line fragment.
---
--- @return LineFragment
function LineFragment:new()
    local obj = {}
    -- Reassign values here to ensure each instance has their own table. If we don't do this, all
    -- instances share *the same content table* and `table.insert` in e.g. `push()` will modify
    -- the state *of all instances*.
    obj.content = {}
    obj.width = 0
    setmetatable(obj, self)
    self.__index = self
    return obj
end

--- Append instances of `LineFragment`.
---
--- @param self LineFragment
--- @param other LineFragment
function LineFragment:append(other)
    return {
        content = vim.fn.extend(self.content, other.content),
        width = self.width + other.width,
    }
end

--- Append a styled string to a line fragment.
---
--- @param self LineFragment
--- @param styled_text StyledText
--- @return LineFragment
function LineFragment:push(styled_text)
    -- NOTE(hartan): Previously this code was using `vim.fn.extend` to concatenate `styled_text` to
    -- `content`, but depending on how I write the constructor in `StyledText` that appears to lose
    -- the metatable along the way, rendering the `StyledText` into a mere struct without methods.
    -- I have *no* clue why that is.
    table.insert(self.content, styled_text)
    self.width = self.width + styled_text.width
    return self
end

--- Convert a line fragment to a printable string with styling applied.
---
--- @param self LineFragment
--- @return string
function LineFragment:to_string()
    local s = ""
    ---@type nil, StyledText
    for _, item in ipairs(self.content) do
        s = s .. item:to_string()
    end
    return s
end

--- Convert a line fragment to a string, truncating from the left.
---
--- @param self LineFragment
--- @param col number The number of columns to skip from the start (0)
--- @return string
function LineFragment:to_string_trunc_left(col)
    local s = ""
    local s_len_remaining = self.width

    if col == 0 then
        return self:to_string()
    end

    -- Iterate in reverse, start from the right
    for idx = vim.tbl_count(self.content), 1, -1 do
        local item = self.content[idx]
        local new_width_remaining = s_len_remaining - item.width

        if new_width_remaining < col then
            local cols_too_narrow = (col - new_width_remaining)
            local truncated = item:to_string_trunc_left(cols_too_narrow)
            return truncated .. s
        elseif new_width_remaining == col then
            return s
        else
            s = item:to_string() .. s
            s_len_remaining = new_width_remaining
        end
    end
    return s
end

--- Convert a line fragment to a string, truncating from the right.
---
--- @param self LineFragment
--- @param col number The number of columns to skip from the end
--- @return string
function LineFragment:to_string_trunc_right(col)
    local s = ""
    local s_len_remaining = self.width

    if col == 0 then
        return self:to_string()
    end

    for _, item in ipairs(self.content) do
        local new_width_remaining = s_len_remaining - item.width

        if new_width_remaining < col then
            local cols_too_narrow = (col - new_width_remaining)
            local truncated = item:to_string_trunc_right(cols_too_narrow)
            return s .. truncated
        elseif new_width_remaining == col then
            return s
        else
            s = s .. item:to_string()
            s_len_remaining = new_width_remaining
        end
    end
    return s
end

return LineFragment
