local M = {
    --- look-up-table for cached highlight groups.
    lut = {},
    --- List of custom keys/highlight groups registered in nvim. This is kept track of so we know
    --- which hls to delete in `M:clear()`.
    lut_custom_keys = {},
    --- If true, the initial `setup` call has been performed.
    initial_setup_done = false,
}

--- Get a default highlight group.
---
--- This isn't meant to win a design contest but to annoy the user to such an extent that they set
--- the required theme colors. I don't want to figure this out on the users behalf.
function M:default_hl()
    return {
        bg = "#ff0000",
        fg = "#00ff00",
    }
end

--- Get a highlight group by name.
---
--- Previously requested results are kept in a LUT for increased lookup performance.
---
--- @param hl_name string Name of the highlight group to obtain
--- @return table? The highlight group for the given input, if found.
function M:get_hl(hl_name)
    if (self.lut[hl_name] == nil) or (next(self.lut[hl_name]) == nil) then
        local hl = vim.api.nvim_get_hl(0, { name = hl_name })
        if (hl == nil) or (next(hl) == nil) then
            return nil
        end
        self.lut[hl_name] = hl
        return hl
    else
        return self.lut[hl_name]
    end
end

--- Register a new highlight group in the LUT and nvim API.
---
--- @param hl_name string The name of the highlight group
--- @param def table The highlight definition.
function M:set_hl(hl_name, def)
    if (def == nil) or (next(def) == nil) then
        self.lut[hl_name] = nil
        vim.api.nvim_set_hl(0, hl_name, {})
    else
        self.lut[hl_name] = def
        vim.api.nvim_set_hl(0, hl_name, def)
        table.insert(self.lut_custom_keys, hl_name)
    end
end

--- Clear all color mappings from the internal LUT and nvim API.
function M:clear()
    for _, key in ipairs(self.lut_custom_keys) do
        vim.api.nvim_set_hl(0, key, {})
    end
    self.lut = {}
end

--- Module setup function.
---
--- Register callback for "Callback" nvim event on first call. Define required highlight groups
--- from default nvim highlight groups.
function M:setup()
    local inv_hl = self:get_hl("TabLineSelInverse")
    if inv_hl == nil then
        local sel_theme = self:get_hl("TabLineSel")
        if sel_theme == nil then
            vim.schedule_wrap(function()
                vim.api.nvim_notify(
                    "highlight group 'TabLineSel' is undefined, using fallback",
                    vim.log.levels["WARN"],
                    {}
                )
            end)
            sel_theme = self:default_hl()
        end
        inv_hl = vim.deepcopy(sel_theme)
        inv_hl.reverse = (not inv_hl.reverse) or true
        -- FIXME(hartan): This still doesn't quite work as I like it to.
        self:set_hl("TabLineSelInverse", inv_hl)
    end

    if not self.initial_setup_done then
        vim.api.nvim_create_autocmd(
            "ColorScheme",
            {
                callback = function()
                    self:clear()
                    self:setup()
                end,
            }
        )
        self.initial_setup_done = true
    end
end

return M
