local Log = {}

--- @class Flags
--- @field once boolean If set to true, message is logged only once

--- Default flags for log messages.
Log.default_flags = {
    --- Set log title to plugin name.
    title = "tabline",
}

--- LUT for messages that have been logged with the `once` flag set.
Log.logged_once = {}

--- Check for the `once` flag in log flags.
---
--- @return boolean _ true if `once` flag is set and true, false otherwise.
function Log:_flag_log_once(flags)
    if flags == nil then
        return false
    else
        return (flags.once or false)
    end
end

--- Mark a message as logged.
---
--- @return boolean _ true if message hasn't been logged before, false otherwise.
function Log:_mark_logged(msg)
    if self.logged_once[msg] == true then
        return false
    else
        self.logged_once[msg] = true
        return true
    end
end

--- Merge given log flags with default flags.
---
--- This currently ensures that the title for log messages is set correctly.
---
--- @return nil _ the flags are merged into the input table.
function Log:_merge_default_flags(flags)
    if flags then
        return vim.tbl_deep_extend("keep", flags, self.default_flags)
    else
        return vim.deepcopy(self.default_flags)
    end
end

function Log:debug(msg, flags)
    self:log(msg, vim.log.levels["DEBUG"], flags)
end

function Log:info(msg, flags)
    self:log(msg, vim.log.levels["INFO"], flags)
end

function Log:warn(msg, flags)
    self:log(msg, vim.log.levels["WARN"], flags)
end

function Log:error(msg, flags)
    self:log(msg, vim.log.levels["ERROR"], flags)
end

function Log:log(msg, level, flags)
    if self:_flag_log_once(flags) then
        if not self:_mark_logged(msg) then
            return
        end
        msg = msg .. "\nThis message is reported only once."
    end
    flags = self:_merge_default_flags(flags)
    -- We mustn't change the UI during tabline rendering, so we schedule
    -- execution for later.
    vim.schedule(function()
        vim.api.nvim_notify(msg, level, flags)
    end)
end

return Log
