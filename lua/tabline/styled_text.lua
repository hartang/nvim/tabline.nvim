-- Copyright (C) 2024 Andreas Hartmann <hartan@7x.de>
-- GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
-- SPDX-License-Identifier: GPL-3.0-or-later

--- @class StyledText
--- @field content string The raw string to be displayed
--- @field width number The width of the content when printed
--- @field style string Name of a nvim highlight group
local StyledText = {
    content = "",
    width = 0,
    style = "",
}

--- Create a new styled text from a string and highligh name.
---
--- @param text string The string to be styled.
--- @param style string The style (highlight group name) to apply.
--- @return StyledText
function StyledText:new(text, style)
    --self.content = text
    --self.style = style
    --self.width = vim.fn.strwidth(text)
    --return self

    -- FIXME(hartan): So the Lua manual (https://www.lua.org/pil/16.1.html) and
    -- some other sources around the internet say this is the way to do it. But
    -- when I do that the metatable disappears upon return and I don't know
    -- where it goes...?
    local obj = {
        content = text,
        style = style,
        width = vim.fn.strwidth(text),
    }
    setmetatable(obj, self)
    self.__index = self
    return obj
end

--- Turn a styled text into a printable string with styling applied.
---
--- @param self StyledText The styled text instance to convert.
--- @return string The formatted string ready for printing.
function StyledText:to_string()
    return "%#" .. self.style .. "#" .. self.content
end

--- Turn a styled text into a printable string and truncate from the left.
---
--- @param self StyledText The text to convert.
--- @param col number Number of columns/chars to suppress
--- @param ellipsis string? If present, prepend this argument as ellipsis
function StyledText:to_string_trunc_left(col, ellipsis)
    local more = ""
    if ellipsis then
        more = ellipsis
        col = col + vim.fn.strwidth(more)
    end

    if col <= 0 then
        return self:to_string()
    elseif col >= self.width then
        return ""
    end

    local strpart = vim.fn.strcharpart(self.content, col)
    return "%#" .. self.style .. "#" .. more .. strpart
end

--- Turn a styled text into a printable string and truncate from the right.
---
--- @param self StyledText The text to convert.
--- @param col number Number of columns/chars to suppress
--- @param ellipsis string? If present, append this argument as ellipsis
function StyledText:to_string_trunc_right(col, ellipsis)
    local more = ""
    if ellipsis then
        more = ellipsis
        col = col + vim.fn.strwidth(more)
    end

    if col <= 0 then
        return self:to_string()
    elseif col >= self.width then
        return ""
    end

    local strpart = vim.fn.strcharpart(self.content, 0, self.width - col)
    return "%#" .. self.style .. "#" .. strpart .. more
end

return StyledText
