local Fs = {
    -- mapping between bufnr and tabribbon
    file_lut = {},
    -- last registered cwd, as fully expanded path
    last_cwd = nil,
    -- List of files in the cwd for quick lookup
    files_in_cwd = {},
    -- Homedir of the user for path shortening
    user_home = nil,
    -- Marker to check if one-time setup was performed
    initial_setup_done = false,
}

--- Initially setup filesystem helpers.
function Fs:setup()
    self.user_home = vim.fn.expand("~")
    self:update_cwd()

    if not self.initial_setup_done then
        vim.api.nvim_create_autocmd(
            "DirChangedPre",
            {
                pattern = "global",
                desc = "tabline cwd update hook",
                callback = function(arg)
                    self:update_cwd(arg.file)
                end,
            }
        )
        self.initial_setup_done = true
    end
end

--- Update the stored cwd.
---
--- Buffer name shortening relies on the cwd to provide unique filenames. If the cwd changes,
--- entries must be invalidated.
--- @param path string? If present, the new cwd to use.
function Fs:update_cwd(path)
    local cwd = path or vim.fn.getcwd()
    if cwd ~= self.last_cwd then
        self.last_cwd = cwd
        self.file_lut = {}
        self.files_in_cwd = {}
        for direntry in vim.fs.dir(cwd, { depth = 1 }) do
            table.insert(self.files_in_cwd, direntry)
        end
    end
end

--- Shorten a filepath.
---
--- @param path string The directory path to shorten without a file component.
--- @return string _ the shortened file path.
function Fs:shorten_filepath(path)
    -- After this operation, the path either starts with:
    --  * '.' if it is beneath the cwd
    --  * '~' if it is beneath $HOME
    --  * '/' if it is neither of the above (absolute path)
    path = vim.fn.fnamemodify(path .. "/x", ":.")
    -- Don't ask me why, but using fnamemodify with ":~:." does **not** result in "the shortest
    -- possible path". It always prefers "~", which is completely pointless for most of what I
    -- need.
    path = path:gsub("^" .. vim.pesc(self.user_home), "~")

    local ret = vim.fn.pathshorten(path, 2)
    return ret:gsub("x$", "")

    -- TODO(hartan): Keeping this for later, see "FIXME" below for why it's broken.
    --local s = ""
    --local components = vim.split(path, "/", { trimempty = false })

    --for idx, component in ipairs(components) do
    --    if idx == 1 and component == "" then
    --        -- If component == "", we still append the leading "/" for absolute paths
    --        s = "/"
    --    elseif idx == 1 and component == "~" then
    --        s = "~/"
    --    elseif component == "" then
    --        -- Two '/' in a row, skip this
    --    else
    --        -- FIXME(hartan): This appears to iterate the path components alright, but I suspect it
    --        -- fails to read the directory entries in `readdir`. In any case the net result is
    --        -- that, at the moment, it returns the input path mostly unmodified, but in
    --        -- complicated.
    --        local short_component = ""
    --        local files_in_s = vim.fn.readdir(s, vim.fn.isdirectory)
    --        for char in component:gmatch(".") do
    --            short_component = short_component .. char
    --            local results = vim.tbl_filter(function(dir)
    --                return dir:find("^" .. short_component)
    --            end, files_in_s)
    --            if (vim.tbl_count(results) == 1) then
    --                if results[1] == component then
    --                    s = s .. short_component .. "/"
    --                    break
    --                else
    --                    vim.notify(
    --                        "Got unexpected result '" .. results[1] ..
    --                        "' while searching for '" .. component ..
    --                        "' beneath '" .. s .. "'",
    --                        vim.log.levels["WARN"], {}
    --                    )
    --                    return path
    --                end
    --            elseif results == {} then
    --                vim.notify(
    --                    "Search for fragment '" .. short_component ..
    --                    "' beneath '" .. s .. "' turned up no results",
    --                    vim.log.levels["ERROR"], {}
    --                )
    --                return path
    --            else
    --                -- More than one result, continue
    --            end
    --        end

    --        s = s .. component .. "/"
    --    end
    --end

    --return s
end

--- Return true if path exists and is a file.
---
--- @param path string The file path to check.
--- @return boolean _ true if path is a file and readable, false otherwise.
function Fs:exists(path)
    return vim.fn.filereadable(path) ~= 0
end

--- Return true if path is beneath the cwd
---
--- @param path string The path to check.
--- @return boolean _ true if path is beneath the cwd, false otherwise.
function Fs:beneath_cwd(path)
    local full_path = vim.fn.fnamemodify(path, ":p")
    return full_path:find("^" .. vim.pesc(self.last_cwd)) ~= nil
end

--- Convert a buffer name to a tab name.
---
--- Applies name shortening if appropriate and keeps track of previously opened buffers to prevent
--- duplicate names.
---
--- @param bufname string The name of the buffer to inspect
--- @return string _ The shortened name to use as tab name
function Fs:buffer_to_tabname(bufname)
    local ret = ""
    if self.file_lut[bufname] ~= nil then
        -- There is a cached entry, return it
        ret = self.file_lut[bufname]
    else
        local basefile = vim.fn.fnamemodify(bufname, ":t")
        if vim.tbl_contains(vim.tbl_values(self.file_lut), basefile) or
            self:exists(self.last_cwd .. "/" .. basefile) or
            not self:beneath_cwd(bufname) then
            -- Either another buffer already uses that "shortened" name (first check) OR
            -- there's a file right inside the cwd that has the same name. In this case we would
            -- generate ambiguous tabnames when first opening another file and then a file with the
            -- same filename inside the cwd.
            ret = self:shorten_filepath(vim.fn.fnamemodify(bufname, ":h")) .. basefile
        else
            -- Use the shortened name
            ret = basefile
        end
        self.file_lut[bufname] = ret
    end

    return ret
end

return Fs
